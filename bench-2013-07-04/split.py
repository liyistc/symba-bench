#!/usr/bin/env python

import os
import os.path
import sys
import glob

def main (argv):
  dir = argv[1]
 
  file = open (dir,'r') 
  for line in file:
    root = os.path.dirname(line)
    base = os.path.basename(line)
    
    optr = root.replace ('/symb/', '/opti/')
    optb = base.replace ('.smt2', '')

    relate = glob.glob (os.path.join (optr,optb).strip() + '*.smt2')
    
    for opts in relate:
      print opts


  print 'Done!'


if __name__ == '__main__':
  sys.exit (main (sys.argv))
