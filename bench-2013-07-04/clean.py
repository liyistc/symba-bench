#!/usr/bin/env python

# Try to locate empty files in both benchmark set
# Discard benchmark if any empty file is found.
# Script takes in the bench generation output path

import os
import os.path
import sys
import glob
from collections import defaultdict


def main (argv):
    dir = argv[1]

    bench = defaultdict(list)

    for root, _, files in os.walk(dir + "/symb"):
        for f in files:
            r_prefix = root.replace ('symb', 'opti')
            f_prefix = f.replace ('.smt2', '')
            relate = glob.glob (os.path.join(r_prefix,f_prefix).strip() + '*')

            if os.stat(os.path.join(root,f)).st_size == 0:
                print "Empty File Found: " + f
#                os.remove (os.path.join(root,f))
                for optis in relate:
                    print "Remove related file: " + optis
#                    os.remove (optis)
                
                continue
            
#print os.path.join(r_prefix,f_prefix)

            opti_bench_set = list()
            bench[os.path.join(root,f)] = opti_bench_set

            
            for optis in relate: 
                if os.stat(optis).st_size == 0:
                    print "Empty File Found: " + optis
                    print "Remove related file: " + os.path.join(root,f)
                    
                    for re in relate:
                        print "Remove related file: " + re
#                       os.remove (re)
                    
#                   os.remove (os.path.join(root,f))
                    del bench[os.path.join(root,f)]
                    break
                    
                bench[os.path.join(root,f)].append (optis)
            
    #print bench
    path_symb = open ('bench_symb', 'w')
    path_opti = open ('bench_opti', 'w')
    
    for key, value in bench.items():
        path_symb.write (key + '\n')
        for f in value:
            path_opti.write (f + '\n')
            

    print 'Done!'

if __name__ == '__main__':
    sys.exit (main (sys.argv))
