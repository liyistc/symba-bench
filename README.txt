=========================================================================

Benchmarks for the symbolic optimization solver Symba in SMT-LIB2 format.

Purpose:

Given a formula \f and a set of optimization objectives
T={t_1, ..., t_n}, the goal is to compute a vector (k_1, ..., k_n),
such that for each t_i, \f => t_i <= k_i and there does not exist
k_i' < k_i where \f => t_i <= k_i'. In another words, we compute the
tightest upper bound for each t_i given the constraint \f.

=========================================================================

Structure of a benchmarks file:

A benchmark file is encoded as an implication from formula \f to the
conjunction of a set of optimization objectives T, i.e.,

(=>
   <Formula f in QF_LRA>
   (and (> t_1 k_1) ... (> t_n k_n))
)

, where t_i (1<=i<=n, n=|T|) are variables/terms (objectives) in T.

=========================================================================

Contents in this directory:

POST queries made by the model checker UFO in verifying C programs from
the SV-COMP-13 benchmark files.
We collected all in-scope variables as templates/objectives.

Categories: ControlFlowInteger,
            DeviceDrivers64,
            ProductLines,
            SystemC.

/bench2: 295 files generated in 2012 (used a shorter timeout).

/bench-2013-07-04: benchmarks generated using BOXES domain
/bench-2013-07-07: benchmakrs generated using BOUND domain
  /opti preprocessed subproblems for optimathsat
  /symb original problems for symba
    *.smt2: original formulas
    *.closed.smt2: closed formulas (eliminate strict inequalities)

/res: results computed by Symba.
